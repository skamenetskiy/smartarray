SmartArray
======

SmartArray is a small yet powerful library providing you a simple array implementation with additional methods we all
needed.

Methods
------

* push
* each
* addEventListener
* removeEventListener
* dispatchEvent
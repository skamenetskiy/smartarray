/**
 * Created by Semen Kamenetskiy (skamenetskiy@live.com) on 19/04/14.
 */

(function () {

    /**
     *
     * @type {window|*|{}}
     */
    var root = window || exports || {};

    /**
     * SmartArray constructor
     * @constructor
     */
    function SmartArray() {
        for (var i = 0; i < arguments.length; i++) {
            this._push(arguments[i])
        }
    }

    /**
     * Extending original array
     * @type {Array}
     */
    SmartArray.prototype = [];

    /**
     * Saving original push method
     * @type {Function}
     * @private
     */
    SmartArray.prototype._push = SmartArray.prototype.push;

    /**
     * Events container
     * @type {{}}
     * @private
     */
    SmartArray.prototype._events = {};

    /**
     * Add new event listener
     * @param event
     * @param handler
     * @expose
     */
    SmartArray.prototype.addEventListener = function (event, handler) {
        if (!(event in this._events)) {
            this._events[event] = [];
        }

        if ('function' != typeof(handler)) {
            throw new TypeError('handler is not a function')
        }

        this._events.push(handler);

        return this
    };

    /**
     * Remove event listener
     * @param event
     * @param handler
     * @expose
     */
    SmartArray.prototype.removeEventListener = function (event, handler) {
        if (event in this._events) {
            for (var i in this._events[event]) {
                if (this._events[event].hasOwnProperty(i) && handler === this._events[event][i]) {
                    delete this._events[event][i];

                    return true
                }
            }
        }

        return false
    };

    /**
     * Dispatches an event
     * @param event
     * @param result
     * @expose
     */
    SmartArray.prototype.dispatchEvent = function (event, result) {
        if (event in this._events) {
            for (var i in this._events[event]) {
                if (this._events[event].hasOwnProperty(i)) {
                    this._events[event][i].call(this, result || undefined);
                }
            }
        }

        return this
    };

    /**
     * Push an element into array
     * @returns {SmartArray}
     * @expose
     */
    SmartArray.prototype.push = function () {
        var result = this._push.apply(this, arguments);
        this.dispatchEvent('push', this);

        return this
    };

    /**
     * foreach implementation
     * @param callback
     * @returns {SmartArray}
     * @expose
     */
    SmartArray.prototype.each = function (callback) {
        if ('function' != typeof(callback)) {
            return this;
        }

        for (var i in this) {
            if (this.hasOwnProperty(i)) {
                callback(this[i], i)
            }
        }

        return this
    };

    /**
     * Define SmartArray as a global object
     * @type {SmartArray}
     */
    root['SmartArray'] = SmartArray;

})();
